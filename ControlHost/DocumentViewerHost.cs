﻿using Shared.FunctionalPattern;
using System.IO;
using System.Windows;
using System.Windows.Forms.Integration;

namespace WpfControlLibrary.ControlHost
{
    public class DocumentViewerHost : WindowsFormsHost
    {
        #region Fields

        public static readonly DependencyProperty DocumentPathProperty = DependencyProperty.Register(
         "DocumentPath", typeof(string), typeof(DocumentViewerHost), new PropertyMetadata(DocumentPathPropertyChanged));

        #endregion Fields

        #region Properties + Indexers

        public string DocumentPath
        {
            get => (string)GetValue(DocumentPathProperty);

            set => SetValue(DocumentPathProperty, value);
        }

        #endregion Properties + Indexers

        #region Methods

        private static void DocumentPathPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(e.NewValue == null)
                return;

            new Guardian().CheckObjectList(d, e);

            var host = (DocumentViewerHost)d;

            var file = new FileInfo((string)e.NewValue);

            if(file.Extension != ".pdf")
                return;

            host.Child = new PdfViewer
            {
                PdfFilePath = file.FullName
            };
        }

        #endregion Methods
    }
}