﻿using Shared.FunctionalPattern;
using System.IO;
using System.Linq;
using System.Windows;

namespace WpfControlLibrary.ControlHost
{
    /// <summary>
    ///    Interaction logic for WebBrowser.xaml
    /// </summary>
    /// <seealso cref="System.Windows.Controls.UserControl"/>
    /// <seealso cref="System.Windows.Markup.IComponentConnector"/>
    /// <seealso cref="IViewerContent"/>
    public partial class WebContent : IViewerContent
    {
        #region Fields + Events

        /// <summary>
        /// The content path property
        /// </summary>
        public static readonly DependencyProperty DocumentPathProperty = DependencyProperty.Register(
         "DocumentPath", typeof(string), typeof(WebContent), new PropertyMetadata(ContentPathPropertyChanged));

        /// <summary>
        /// The helper
        /// </summary>
        private static ViewerContentHelper _helper = new ViewerContentHelper();

        #endregion Fields + Events

        #region Properties + Indexers

        /// <summary>
        /// Gets or sets the content path.
        /// </summary>
        /// <value>The content path.</value>

        public string DocumentPath
        {
            get => (string)GetValue(DocumentPathProperty);

            set
            {
                _helper = GetHelper(value, ".html", ".htm");
                SetValue(DocumentPathProperty, value);
            }
        }

        /// <summary>
        /// Gets the browser builder.
        /// </summary>
        /// <value>The browser builder.</value>
        private WebContentBuilder BrowserBuilder { get; set; }

        #endregion Properties + Indexers

        #region Constructors + Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WebContent"/> class.
        /// </summary>
        public WebContent()
        {
            InitializeComponent();
            BrowserBuilder = new WebContentBuilder(gridwebBrowser);
        }

        #endregion Constructors + Destructors

        #region Methods

        /// <summary>
        /// Contents the path property changed.
        /// </summary>
        /// <param name="dependencyObject">The dependency object.</param>
        /// <param name="args">
        /// The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.
        /// </param>
        private static void ContentPathPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
        {
            new Guardian().CheckObjectList(dependencyObject, args);
            if (args.NewValue == null)
                return;

            var host = (WebContent)dependencyObject;
            host.BrowserBuilder.A_NewWebBrowser(_helper.File.FullName);
        }

        /// <summary>
        /// Gets the helper.
        /// </summary>
        /// <param name="path">     The path.</param>
        /// <param name="validExts">The valid exts.</param>
        /// <returns></returns>
        private static ViewerContentHelper GetHelper(string path, params string[] validExts)
        {
            return new ViewerContentHelper
            {
                ValidExtensions = validExts.ToList(),
                File = new FileInfo(path)
            };
        }

        #endregion Methods
    }
}