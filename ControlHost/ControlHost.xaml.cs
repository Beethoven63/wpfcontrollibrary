﻿using Shared.FunctionalPattern;
using Shared.Logging.Responses;
using System.IO;
using System.Windows;

namespace WpfControlLibrary.ControlHost
{
    /// <summary>Interaction logic for ControlHost.xaml</summary>
    /// <seealso cref="System.Windows.Controls.UserControl"/>
    /// <seealso cref="System.Windows.Markup.IComponentConnector"/>
    public partial class ControlHost
    {
        #region Fields

        /// <summary>The file path property</summary>
        public static readonly DependencyProperty FilePathProperty
            = DependencyProperty.Register(
                                          "FilePath",
                                          typeof(string),
                                          typeof(ControlHost),
                                          new FrameworkPropertyMetadata(FilePathChanged));

        private static readonly Guardian Guardian = new Guardian();

        /// <summary>The view content</summary>
        private IViewerContent _viewContent;

        #endregion

        #region Properties + Indexers

        /// <summary>Gets or sets the file path.</summary>
        /// <value>The file path.</value>
        public string FilePath
        {
            get => (string)GetValue(FilePathProperty);
            set => SetValue(FilePathProperty, value);
        }

        /// <summary>Gets or sets the content of the view.</summary>
        /// <value>The content of the view.</value>
        public IViewerContent ViewContent
        {
            get => _viewContent;

            set
            {
                _viewContent = value;
                Content = value;
            }
        }

        #endregion

        #region Constructors + Destructors

        /// <summary>Initializes a new instance of the <see cref="ControlHost"/> class.</summary>
        public ControlHost()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>Assigns the content of the view.</summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private static BaseResponse<IViewerContent> AssignViewContent(string path)
        {
            var file = new FileInfo(path);

            switch (file.Extension)
            {
                case ".pdf":
                    return GetPdfContent(file);

                case ".txt":
                    return GetTextContent(file);

                case "wmv":
                case ".mp3":
                    return GetMediaContent(file);

                case ".html":
                case ".htm":
                    return GetWebContent(file);

                default:
                    return new BaseResponse<IViewerContent>();
            }
        }

        /// <summary>Files the path changed.</summary>
        /// <param name="d">The d.</param>
        /// <param name="e">
        /// The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.
        /// </param>
        private static void FilePathChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null)
                return;

            var host = (ControlHost)d;

            Guardian.CheckObject(host);

            var response = AssignViewContent(e.NewValue.ToString());
            if (!response.Success)
                return;

            host.ViewContent = response.Data;
            Guardian.CheckObject(host.ViewContent);

            host.PlayContent();

            //else
            //{
            //    //file.OpenContainingDirectory();
            //}
        }

        private static BaseResponse<IViewerContent> GetMediaContent(FileInfo file)
        {
            return new BaseResponse<IViewerContent>()
            {
                Success = true,
                Data = new MediaContent { DocumentPath = file.FullName }
            };
        }

        private static BaseResponse<IViewerContent> GetPdfContent(FileInfo file)
        {
            return new BaseResponse<IViewerContent>()
            {
                Success = true,
                Data = new PdfContent { DocumentPath = file.FullName }
            };
        }

        private static BaseResponse<IViewerContent> GetTextContent(FileInfo file)
        {
            return new BaseResponse<IViewerContent>()
            {
                Success = true,
                Data = new TextContent { DocumentPath = file.FullName }
            };
        }

        private static BaseResponse<IViewerContent> GetWebContent(FileInfo file)
        {
            return new BaseResponse<IViewerContent>()
            {
                Success = true,
                Data = new WebContent { DocumentPath = file.FullName }
            };
        }

        /// <summary>Plays the content.</summary>
        private void PlayContent()
        {
            var media = ViewContent as MediaContent;

            media?.PlayMedia(() => Background);
        }

        #endregion
    }
}