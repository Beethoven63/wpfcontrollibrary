﻿using Shared.FilesAndDirectories;
using Shared.Validation;
using System;
using System.IO;
using System.Linq;

namespace WpfControlLibrary.ControlHost
{
    public class BaseContent : IViewerContent
    {
        #region Fields

        private readonly Validator _v = new Validator();
        private string _contentPath;

        #endregion

        #region Properties + Indexers

        public string DocumentPath
        {
            get => _contentPath;

            set
            {
                _contentPath = value;
                File = new FileInfo(DocumentPath);
            }
        }

        public FileInfo File { get; set; }

        public System.Collections.Generic.List<string> ValidExtensions { get; set; }

        #endregion

        #region Methods

        public void ValidateContent()
        {
            var valid = _v.IsValid(() => File.CheckFile(),
                                   () => ValidExtensions.Any(),
                                   () => ValidExtensions.Contains(File.Extension));

            if (!valid)
                throw new Exception("Content is invalid");
        }

        #endregion
    }
}