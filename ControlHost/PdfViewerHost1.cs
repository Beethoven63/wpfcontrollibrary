﻿using System.IO;
using System.Windows;
using System.Windows.Forms.Integration;

namespace Utility.WPF
{
    public class PdfViewerHost : WindowsFormsHost
    {
        #region Fields

        public static readonly DependencyProperty PdfPathProperty = DependencyProperty.Register(
         "PdfPath", typeof(string), typeof(PdfViewerHost), new PropertyMetadata(PdfPathPropertyChanged));

        private readonly PdfViewer wrappedControl;

        #endregion

        #region Properties + Indexers

        public string PdfPath
        {
            get
            {
                return (string)GetValue(PdfPathProperty);
            }

            set
            {
                SetValue(PdfPathProperty, value);
            }
        }

        #endregion

        #region Constructors

        public PdfViewerHost()
        {
            wrappedControl = new PdfViewer();
            Child = wrappedControl;
        }

        #endregion

        #region Methods

        private static void PdfPathPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var host = (PdfViewerHost)d;

            var file = new FileInfo((string)e.NewValue);
            if (file.Extension == ".pdf")
                host.wrappedControl.PdfFilePath = file.FullName;
            else
            {
                host.wrappedControl.Enabled = false;
            }
        }

        #endregion
    }
}
