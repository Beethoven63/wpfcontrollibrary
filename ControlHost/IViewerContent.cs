﻿namespace WpfControlLibrary.ControlHost
{
    public interface IViewerContent
    {
        #region Properties + Indexers

        string DocumentPath { get; set; }

        #endregion Properties + Indexers
    }
}