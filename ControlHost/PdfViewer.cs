﻿using System.Windows.Forms;

namespace WpfControlLibrary.ControlHost
{
    public partial class PdfViewer : UserControl
    {
        #region Fields

        private string _pdfFilePath;

        #endregion Fields

        #region Properties + Indexers

        public string PdfFilePath
        {
            get => _pdfFilePath;

            set
            {
                if (string.IsNullOrEmpty(value))
                    return;

                if (_pdfFilePath == value)
                    return;

                _pdfFilePath = value;
                ChangeCurrentDisplayedPdf();
            }
        }

        #endregion Properties + Indexers

        #region Constructors + Destructors

        public PdfViewer()
        {
            InitializeComponent();
            AcrobatViewer.setShowToolbar(false);
            AcrobatViewer.setView("FitH");
        }

        #endregion Constructors + Destructors

        #region Methods

        public void Print()
        {
            AcrobatViewer.printWithDialog();
        }

        private void ChangeCurrentDisplayedPdf()
        {
            //acrobatViewer.LoadFile(PdfFilePath);
            AcrobatViewer.src = PdfFilePath;
            AcrobatViewer.setViewScroll("FitH", 0);
        }

        #endregion Methods
    }
}