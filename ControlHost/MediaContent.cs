﻿using Shared.FilesAndDirectories;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace WpfControlLibrary.ControlHost
{
    /// <summary>
    /// </summary>
    /// <seealso cref="IViewerContent" />
    public class MediaContent : IViewerContent
    {
        #region Fields

        /// <summary>
        ///    The helper
        /// </summary>
        private readonly ViewerContentHelper _helper;

        #endregion

        #region Properties + Indexers

        /// <summary>
        ///    Gets or sets the content path.
        /// </summary>
        /// <value> The content path. </value>
        public string DocumentPath { get; set; }

        #endregion

        #region Constructors + Destructors

        /// <summary>
        ///    Initializes a new instance of the <see cref="MediaContent" /> class.
        /// </summary>
        public MediaContent()
        {
            _helper = new ViewerContentHelper
            {
                ValidExtensions = new List<string> { "wmv", ".mp3" },
                File = new System.IO.FileInfo(DocumentPath)
            };
        }

        #endregion

        #region Methods

        /// <summary>
        ///    Plays the media.
        /// </summary>
        /// <param name="getBackground"> The get background. </param>
        public void PlayMedia(Func<Brush> getBackground)
        {
            _helper.ValidateContent();

            var mp = new MediaPlayer();

            mp.Open(_helper.File.ToUri());

            getBackground();
            var brush = new DrawingBrush(new VideoDrawing
            {
                Player = mp,
                Rect = new Rect(0, 0, 100, 100)
            });

            mp.Play();
        }

        #endregion
    }
}