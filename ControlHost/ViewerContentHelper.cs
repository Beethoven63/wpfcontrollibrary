﻿using Shared.FilesAndDirectories;
using Shared.Validation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WpfControlLibrary.ControlHost
{
    public class ViewerContentHelper : IViewerContent
    {
        #region Fields

        private readonly Validator _v = new Validator();
        private string _contentPath;

        #endregion

        #region Properties + Indexers

        public string DocumentPath
        {
            get => _contentPath;

            set
            {
                _contentPath = value;
                File = new FileInfo(DocumentPath);
            }
        }

        public FileInfo File { get; set; }

        public List<string> ValidExtensions { get; set; }

        #endregion

        #region Methods

        public void ValidateContent()
        {
            var valid = _v.IsValid(
                () => File.CheckFile(),
                () => ValidExtensions.Any(),
                () => ValidExtensions.Contains(File.Extension));

            if (!valid)
                throw new Exception("Content is invalid");
        }

        #endregion
    }
}