using Shared.FilesAndDirectories;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace WpfControlLibrary.ControlHost
{
    public class TextContentBuilder
    {
        #region Fields

        private readonly Grid _hostGrid;

        #endregion

        #region Properties + Indexers

        private TextBlock TextBlock { get; set; }

        #endregion

        #region Constructors

        public TextContentBuilder(Grid uiElement)
        {
            _hostGrid = uiElement;
        }

        #endregion

        #region Methods

        public void NewTextView(string path)
        {
            InitViewer();

            ReadFile(path);
        }

        private void InitViewer()
        {
            if (TextBlock != null)
            {
                _hostGrid.Children.Remove(TextBlock);
            }
            TextBlock = new TextBlock
            {
                TextWrapping = TextWrapping.Wrap
            };

            _hostGrid.Children.Add(TextBlock);
        }

        private void ReadFile(string url)
        {
            TextBlock.Text = new FileInfo(url).Read();
        }

        #endregion
    }
}