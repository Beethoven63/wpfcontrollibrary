﻿using Shared.Collection;
using Shared.FunctionalPattern;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Forms.Integration;

namespace WpfControlLibrary.ControlHost
{
    /// <summary>
    /// </summary>
    /// <seealso cref="WindowsFormsHost"/>
    /// <seealso cref="IViewerContent"/>
    public class PdfContent : WindowsFormsHost, IViewerContent
    {
        #region Fields + Events

        /// <summary>
        /// The content path property
        /// </summary>
        public static readonly DependencyProperty DocumentPathProperty = DependencyProperty.Register(
            "DocumentPath", typeof(string), typeof(PdfContent), new PropertyMetadata(ContentPathPropertyChanged));

        /// <summary>
        /// The helper
        /// </summary>
        private static ViewerContentHelper _helper = new ViewerContentHelper();

        /// <summary>
        /// The viewer
        /// </summary>
        private readonly PdfViewer _viewer;

        #endregion Fields + Events

        #region Properties + Indexers

        /// <summary>
        /// Gets or sets the content path.
        /// </summary>
        /// <value>The content path.</value>
        public string DocumentPath
        {
            get => (string)GetValue(DocumentPathProperty);

            set
            {
                _helper = GetHelper(value, ".pdf");
                SetValue(DocumentPathProperty, value);
            }
        }

        #endregion Properties + Indexers

        #region Constructors + Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PdfContent"/> class.
        /// </summary>
        public PdfContent()
        {
            _viewer = new PdfViewer();
            Child = _viewer;
        }

        #endregion Constructors + Destructors

        #region Methods

        /// <summary>
        /// Casts the specified o.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="o">The o.</param>
        /// <returns></returns>
        private static T Cast<T>(object o)
        {
            return (T)o;
        }

        /// <summary>
        /// Contents the path property changed.
        /// </summary>
        /// <param name="d">The d.</param>
        /// <param name="e">
        /// The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.
        /// </param>
        private static void ContentPathPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            new Guardian().CheckObjectList(d, e);
            if (e.NewValue.ToString().IsNullOrEmpty())
                return;

            _helper.ValidateContent();

            Cast<PdfContent>(d)._viewer.PdfFilePath = _helper.File.FullName;
        }

        /// <summary>
        /// Gets the helper.
        /// </summary>
        /// <param name="path">     The path.</param>
        /// <param name="validExts">The valid exts.</param>
        /// <returns></returns>
        private static ViewerContentHelper GetHelper(string path, params string[] validExts)
        {
            return new ViewerContentHelper
            {
                ValidExtensions = validExts.ToList(),
                File = new FileInfo(path)
            };
        }

        #endregion Methods
    }
}