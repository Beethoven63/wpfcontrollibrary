using mshtml;
using Shared.Collection;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace WpfControlLibrary.ControlHost
{
    public class WebContentBuilder
    {
        #region Fields

        private readonly Grid _hostGrid;

        #endregion

        #region Properties + Indexers

        public HTMLDocument Document { get; set; }
        private WebBrowser Browser { get; set; }

        #endregion

        #region Constructors + Destructors

        public WebContentBuilder(Grid gridwebBrowser)
        {
            _hostGrid = gridwebBrowser;
        }

        #endregion

        #region Methods

        public void A_NewWebBrowser(string url)
        {
            DestructExistingSetup();

            InitBrowser();

            if (url.IsNullOrEmpty())
            {
                CreateNewDocument();
            }
            else
            {
                CreateDocumentFromUrl(url);
            }
        }

        private void CreateDocumentFromUrl(string url)
        {
            Browser.Navigate(url);
            if ((HTMLDocument)Browser.Document != null)
            {
                Document = (HTMLDocument)Browser.Document;
            }
        }

        private void CreateNewDocument()
        {
            Browser.NavigateToString("New");
            Document = (HTMLDocument)Browser.Document;
            Document.designMode = "On";
        }

        private void DestructExistingSetup()
        {
            if (Browser != null)
            {
                Browser.LoadCompleted -= LoadingCompleted;
                Browser.Dispose();
                _hostGrid.Children.Remove(Browser);
            }

            Document?.clear();
        }

        private void InitBrowser()
        {
            Browser = new WebBrowser();
            Browser.LoadCompleted += LoadingCompleted;
            _hostGrid.Children.Add(Browser);

            //Script.HideScriptErrors(Browser, true);
        }

        private void LoadingCompleted(object sender, NavigationEventArgs e)
        {
            Document = (HTMLDocument)Browser.Document;
            if (Document == null)
                return;

            Document.designMode = "On";
        }

        #endregion
    }
}