﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace WpfControlLibrary.ControlHost
{
    partial class PdfViewer
    {
        #region Fields

        private AxAcroPDF acrobatViewer;

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        public AxAcroPDF AcrobatViewer { get => acrobatViewer; set => acrobatViewer = value; }

        #endregion

        #region Methods

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            var resources = new ComponentResourceManager(typeof(PdfViewer));
            this.AcrobatViewer = new AxAcroPDF();
            ((ISupportInitialize)(this.AcrobatViewer)).BeginInit();
            this.SuspendLayout();
            // 
            // acrobatViewer
            // 
            this.AcrobatViewer.Dock = DockStyle.Fill;
            this.AcrobatViewer.Enabled = true;
            this.AcrobatViewer.Location = new Point(0, 0);
            this.AcrobatViewer.Name = "acrobatViewer";
            this.AcrobatViewer.OcxState = ((AxHost.State)(resources.GetObject("acrobatViewer.OcxState")));
            this.AcrobatViewer.Size = new Size(150, 150);
            this.AcrobatViewer.TabIndex = 0;
            // 
            // PdfViewer
            // 
            this.AutoScaleDimensions = new SizeF(6F, 13F);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.Controls.Add(this.AcrobatViewer);
            this.Name = "PdfViewer";
            ((ISupportInitialize)(this.AcrobatViewer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}
