﻿using Ookii.Dialogs.Wpf;
using Shared.FileHandling;
using Shared.FunctionalPattern;
using Shared.Logging.Responses;
using System;
using System.IO;
using System.Windows.Forms;

namespace WpfControlLibrary.FileDialogHelper
{
    /// <summary>
    /// </summary>
    public class FileDialogHelper
    {
        #region Fields

        private readonly FileChanger _changer = new FileChanger();

        /// <summary>
        /// The guardian
        /// </summary>
        private readonly Guardian _guardian = new Guardian();

        #endregion

        #region Methods

        /// <summary>
        /// Requests the data from user.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <typeparam name="TDialog">The type of the dialog.</typeparam>
        /// <param name="request">    The request.</param>
        /// <param name="getResponse">The get response.</param>
        /// <returns></returns>
        public BaseResponse<TData> RequestDataFromUser<TData, TDialog>(UserDataRequest request, Func<TDialog, TData> getResponse = null)
            where TDialog : FileDialog, new()
        {
            _guardian.CheckObjectList(request, getResponse);

            using (var dialog = new TDialog
            {
                Filter = request.Filter,
                FileName = request.TargetFile.FullName,
                InitialDirectory = request.InitialDirectory
            })
            {
                var data = default(TData);
                var temp = dialog;
                var threadReq = new DispatcherRequest<TData>
                {
                    Invocation = () => DiagDisplay(temp, getResponse, out data)
                };

                var response = new ThreadingHelper<TData>().InvokeOnUiByRequest(threadReq);
                return new BaseResponse<TData>
                {
                    Data = data,
                    Success = response.Success
                };
            }
        }

        public BaseResponse<string> RequestDirectoryFromUser(UserDataRequest request)
        {
            _guardian.CheckObjectList(request);

            //Note: No using block because dialog is not disposable
            var dialog = new VistaFolderBrowserDialog
            {
                Description = request.Description,
                UseDescriptionForTitle = true,
                RootFolder = Environment.SpecialFolder.Personal,
                ShowNewFolderButton = true,
                SelectedPath = request.InitialDirectory
            };

            var response = dialog.ShowDialog();

            return new BaseResponse<string>
            {
                Data = dialog.SelectedPath,
                Success = response != null && (bool)response
            };
        }

        public BaseResponse<string> RequestFileFromUser(UserDataRequest request)
        {
            _guardian.CheckObjectList(request);

            using (var dialog = new OpenFileDialog
            {
                Title = request.Description,
                InitialDirectory = request.InitialDirectory,
                Filter = request.Filter,
                RestoreDirectory = true
            })
            {
                var diagResult = dialog.ShowDialog();

                return new BaseResponse<string>
                {
                    Data = dialog.FileName,
                    Success = diagResult == DialogResult.OK
                };
            }
        }

        /// <summary>
        /// Saves as.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BaseResponse<FileInfo> SaveAs(UserDataRequest request)
        {
            _guardian.CheckObject(request);

            using (var dialog = new SaveFileDialog
            {
                Filter = request.Filter,
                FileName = request.TargetFile.FullName,
                InitialDirectory = request.InitialDirectory
            })
            {
                var dialogResult = dialog.ShowDialog();
                if (dialogResult != DialogResult.OK)
                    return new BaseResponse<FileInfo>();

                var changedFile = new FileInfo(dialog.FileName);

                _changer.MoveOnly(originalFile: changedFile, newDirectoryName: changedFile.DirectoryName);

                return new BaseResponse<FileInfo>()
                {
                    Success = changedFile.Exists,
                    Data = changedFile
                };
            }
        }

        /// <summary>
        /// Selects the file.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <typeparam name="TDialog">The type of the dialog.</typeparam>
        /// <param name="request">    The request.</param>
        /// <param name="getResponse">The get response.</param>
        /// <returns></returns>
        public TData SelectFile<TData, TDialog>(UserDataRequest request, Func<TDialog, TData> getResponse)
            where TDialog : FileDialog, new()
        {
            _guardian.CheckObjectList(request, getResponse);
            _guardian.CheckObjectList(request.InitialDirectory);

            using (var dialog = new TDialog
            {
                Filter = request.Filter,
                InitialDirectory = request.InitialDirectory
            })
            {
                var diagResult = dialog.ShowDialog();

                return diagResult == DialogResult.OK ? getResponse(dialog) : default;
            }
        }

        /// <summary>
        /// Displays the dialog.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <typeparam name="TDialog">The type of the dialog.</typeparam>
        /// <param name="dialog">     The dialog.</param>
        /// <param name="getResponse">The get response.</param>
        /// <param name="response">   The response.</param>
        private void DiagDisplay<TData, TDialog>(TDialog dialog, Func<TDialog, TData> getResponse, out TData response)
                            where TDialog : FileDialog, new()
        {
            response = dialog.ShowDialog() == DialogResult.OK
                ? getResponse(dialog)
                : default;
        }

        #endregion
    }
}