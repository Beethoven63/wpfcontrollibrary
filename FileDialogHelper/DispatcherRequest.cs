﻿using Shared.Data.Ado.AdoConveyor.Query;
using System;

namespace WpfControlLibrary.FileDialogHelper
{
    public class DispatcherRequest<TKey> : DataQueryRequest<TKey, TKey>
    {
        #region Properties + Indexers

        public Action Invocation { get; set; }

        #endregion Properties + Indexers
    }
}