﻿using System.Collections.Generic;
using System.IO;

namespace WpfControlLibrary.FileDialogHelper
{
    public class UserDataRequest
    {
        #region Fields

        private static readonly Dictionary<FileFilterType, string> Filters = new Dictionary<FileFilterType, string>();
        private FileFilterType _filterType;

        #endregion

        #region Properties + Indexers

        public string Description { get; set; }

        public string Filter => GetFilterString(_filterType);

        public FileFilterType FilterType
        {
            set => _filterType = value;
        }

        public string InitialDirectory { get; set; }

        public FileInfo TargetFile { get; set; }

        #endregion
        #region Constructors + Destructors

        static UserDataRequest()
        {
            Filters[FileFilterType.Pdf] = "PDF files (*.pdf)|*.pdf|All files (*.*)|*.*";
            Filters[FileFilterType.Mp3] = "MP3 files (*.mp3)|*.mp3|All files (*.*)|*.*";
            Filters[FileFilterType.CS] = "C# class files (*.cs)|*.cs|All files (*.*)|*.*";
            Filters[FileFilterType.Sql] = "Sql Script files (*.sql)|*.sql|All files (*.*)|*.*";
            Filters[FileFilterType.Csv] = "Comma Separated Value Files (*.csv)|*.csv|All files (*.*)|*.*";
            Filters[FileFilterType.Txt] = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            Filters[FileFilterType.Xls] = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
        }

        #endregion

        #region Methods

        private string GetFilterString(FileFilterType type)
        {
            return Filters.ContainsKey(type) ? Filters[type] : string.Empty;
        }

        #endregion
    }
}