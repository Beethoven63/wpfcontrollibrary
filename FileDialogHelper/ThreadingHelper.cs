﻿using Shared.FunctionalPattern;
using Shared.Logging.Responses;
using System;
using System.Windows;

namespace WpfControlLibrary.FileDialogHelper
{
    public class ThreadingHelper<TData>
    {
        #region Fields

        private readonly ExecutionHelper _run = new ExecutionHelper();

        #endregion Fields

        #region Methods

        public void InvokeOnUi(Action invocation)
        {
            if (Application.Current.Dispatcher != null && Application.Current.Dispatcher.CheckAccess() == false)
            {
                Application.Current.Dispatcher.Invoke(invocation);
            }
            else
            {
                invocation();
            }
        }

        public BaseResponse<TData> InvokeOnUiByRequest(DispatcherRequest<TData> request)
        {
            var eRequest = new ExecutionRequest<DispatcherRequest<TData>, BaseResponse<TData>, TData>
            {
                Init = () => new DispatcherRequest<TData>
                {
                    Invocation = request.Invocation
                },
                Action = InvokeAction
            };
            return _run.RequestResponse(eRequest);
        }

        internal void InvokeOnUiByRequest(UserDataRequest request)
        {
            throw new NotImplementedException();
        }

        private BaseResponse<TData> InvokeAction(DispatcherRequest<TData> request)
        {
            //if (Application.Current.Dispatcher.CheckAccess() == false)
            //{
            //    Application.Current.Dispatcher.Invoke(request.Invocation);
            //}
            //else
            //{
            request.Invocation();
            //}

            return new BaseResponse<TData>
            {
                Success = true
            };
        }

        #endregion Methods
    }
}