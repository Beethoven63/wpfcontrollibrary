﻿namespace WpfControlLibrary.FileDialogHelper
{
    public enum FileFilterType
    {
        Pdf,
        Mp3,
        Csv,
        CS,
        Txt,
        Sql,
        Xls
    }
}